use failure::Error;
use pest::Span;
use std::collections::HashMap;
use crate::core::{
    Pass as PassTrait,
    Entity,
    WithTree,
    WithTypes,
    WithSpans,
    WithLiterals,
    WithEntities
};

use crate::ast::output::{Node, Else, ElseIf};

pub mod output;
use self::output::*;

pub mod errors;
use self::errors::*;

#[derive(Default, Debug, Clone)]
struct Context<'a> {
    local_vars: HashMap<&'a str, Entity>,
}

pub struct Pass<'a> {
    output: Output<'a>,
    stack: Vec<Context<'a>>
}

impl <'a> Pass<'a> {
    pub fn new<P>(mut prev: P) -> Self
    where P: WithTree
        + WithEntities
        + WithTypes
        + WithSpans<'a>
        + WithLiterals<'a> {
        let mut output = Output::default();
        output.root = Some(prev.get_tree());
        output.spans = Some(prev.get_spans());
        output.literals = Some(prev.get_literals());
        output.types = Some(prev.get_types());
        output.entities = Some(prev.get_entities());
        output.var_bindings = Some(HashMap::new());
        Pass {
            output,
            stack: vec![
                Context::default()
            ]
        }
    }

    fn add_var(&mut self, entity: Entity, var: VarBinding) {
        self.output
            .var_bindings
            .as_mut()
            .unwrap()
            .insert(entity, var);
    }

    fn get_span(&mut self, entity: Entity) -> &Span<'a> {
        self.output
            .spans
            .as_ref()
            .unwrap()
            .get(&entity).unwrap()
    }

    fn get_last_context(&mut self) -> &mut Context<'a> {
        let stack_len = self.stack.len();
        &mut self.stack[stack_len - 1]
    }

    fn add_local(&mut self, name: &'a str, entity: Entity) {
        let i = *self.output.entities.as_mut().unwrap();
        //let count = context.count;
        self.output.entities = Some(i + 1);
        let context = self.get_last_context();
        context.local_vars.insert(name, i);

        self.add_var(entity, VarBinding(i));
        //context.count += 1;
    }

    fn get_var_binding(&mut self, name: &'a str)
                       -> Result<VarBinding, LocalVarsError> {

        for context in self.stack.iter().rev() {
            if let Some(binding) = context.local_vars.get(name) {
                return Ok(VarBinding(*binding));
            }
        }

        Err(LocalVarsError::VariableNotFound { name: name.to_string() })
    }


    fn new_context(&mut self) {
        let new = Context::default();
        self.stack.push(new);
    }

    fn parse_node(&mut self, node: &Node) -> Result<(), Error> {
        match node {
            Node::Block(_, inner) => {
                self.new_context();

                for node in inner {
                    self.parse_node(node)?;
                }

                self.stack.pop();
            },
            Node::Tuple(_, inner) => {
                for node in inner {
                    self.parse_node(node)?;
                }
            },
            Node::Unary(_, _, inner) => {
                self.parse_node(inner)?;
            },
            Node::Binary(_, _, lhs, rhs) => {
                self.parse_node(lhs)?;
                self.parse_node(rhs)?;
            },
            Node::Let(_, vardecls) => {
                for vardecl in vardecls {
                    let e = vardecl.0;
                    let name = self.get_span(e).as_str();

                    self.add_local(name, e);

                    if let Some(ref var) = vardecl.1 {
                        self.parse_node(&var)?;
                    }
                }
            },
            Node::If(_, cond, block, elseif, els) => {
                self.parse_node(cond)?;
                self.parse_node(block)?;
                for ElseIf(cond, block) in elseif {
                    self.parse_node(cond)?;
                    self.parse_node(block)?;
                }
                if let Some(Else(els)) = els {
                    self.parse_node(els)?;
                }
            },
            Node::Loop(_, block) => {
                self.parse_node(block)?;
            },
            Node::Variable(entity) => {
                let name = self.get_span(*entity).as_str();
                let binding = self.get_var_binding(name)?;
                self.add_var(*entity, binding);
            },
            Node::Print(_, e) => {
                self.parse_node(e)?;
            },
            Node::Break(_, e) => {
                if let Some(e) = e {
                    self.parse_node(e)?;
                }
            },
            Node::Unit(_) => (),
            Node::Literal(_) => ()
        }
        Ok(())
    }
}

impl <'a> PassTrait for Pass<'a> {
    type Output = self::Output<'a>;

    fn process(mut self) -> Result<Self::Output, Error> {
        let root = self.output.root.take().unwrap();
        self.parse_node(&root)?;
        self.output.root = Some(root);
        Ok(self.output)
    }
}
