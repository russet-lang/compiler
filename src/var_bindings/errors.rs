use failure::Fail;

#[derive(Debug, Fail)]
pub enum LocalVarsError {
    #[fail(display="Undeclared variable `{}`", name)]
    VariableNotFound {
        name: String
    }
}
