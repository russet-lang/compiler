use failure::Error;
use crate::core::{
    Pass as PassTrait,
    Entity,
    WithVarBindings,
    WithTree,
    WithSpans,
    WithLiterals,
    WithTypes
};

use crate::ast::output::{Node, ElseIf, Else, Literal, UnaryOp, BinaryOp};
use crate::var_bindings::output::*;
use crate::types::Type;

pub mod errors;
use self::errors::*;

pub struct Pass<'a>
{
    output: Output<'a>
}

impl <'a> Pass<'a> {
    pub fn new<P>(mut prev: P) -> Self
    where P: WithTree
        + WithVarBindings
        + WithTypes
        + WithSpans<'a>
        + WithLiterals<'a> {
        let mut output = Output::default();
        output.root = Some(prev.get_tree());
        output.spans = Some(prev.get_spans());
        output.literals = Some(prev.get_literals());
        output.types = Some(prev.get_types());
        output.var_bindings = Some(prev.get_var_bindings());
        Pass {
            output
        }
    }

    fn get_binding(&mut self, entity: Entity) -> &VarBinding {
        self.output
            .var_bindings
            .as_mut()
            .unwrap()
            .get(&entity)
            .expect("get_binding")
    }

    fn declare_var_type(&mut self, e: Entity, t: Type) {
        let binding = {self.get_binding(e).0};

        self.declare_type(binding, t);

    }

    fn declare_type(&mut self, e: Entity, t: Type) -> Type {
        self.output.types
            .as_mut()
            .expect("types")
            .insert(e, t.clone());
        t
    }

    fn get_var_type(&mut self, e: Entity) -> Result<Type, Error> {
        let binding = {self.get_binding(e).0};

        let t = self.output
            .types
            .as_mut()
            .unwrap()
            .get(&binding)
            .unwrap()
            .clone();

        Ok(t)
    }

    fn get_type(&self, e: Entity) -> Type {
        self.output.types
            .as_ref()
            .expect("types")
            .get(&e)
            .expect("type")
            .clone()
    }

    fn assert_type(&self, ta: Type, tb: Type) -> Result<Type, Error> {
        if ta != tb {
            return Err(TypeError::MismatchedTypes {
                left: ta,
                right: tb
            }.into());
        }
        Ok(ta)
    }

    fn parse_node(&mut self, node: &Node) -> Result<Type, Error> {
        match node {
            Node::Block(e, inner) => {
                let mut t : Option<Type> = None;

                for node in inner {
                    t = Some(self.parse_node(node)?);
                }

                let t = t.unwrap_or(Type::Unit);

                Ok(self.declare_type(*e, t))
            },
            Node::Tuple(e, inner) => {
                let mut t : Vec<Type> = vec![];

                for node in inner {
                    t.push(self.parse_node(node)?);
                }

                let t = Type::Tuple(t);
                Ok(self.declare_type(*e, t))
            },
            Node::Unary(e, UnaryOp::Not, inner) => {
                let t = self.parse_node(inner)?.assert_bool()?;
                Ok(self.declare_type(*e, t))
            },
            Node::Unary(e, UnaryOp::Minus, inner) => {
                let t = self.parse_node(inner)?.assert_i32()?;
                Ok(self.declare_type(*e, t))
            },
            Node::Binary(e, BinaryOp::Larger, lhs, rhs) => {
                let ta = self.parse_node(lhs)?.assert_i32()?;
                let tb = self.parse_node(rhs)?.assert_i32()?;

                self.assert_type(ta, tb)?;
                Ok(self.declare_type(*e, Type::Bool))
            },
            Node::Binary(e, BinaryOp::Assign, lhs, rhs) => {
                let ta = self.parse_node(lhs)?;
                let tb = self.parse_node(rhs)?;

                let t = self.assert_type(ta, tb)?;
                Ok(self.declare_type(*e, t))
            },
            Node::Binary(e, _, lhs, rhs) => {
                let ta = self.parse_node(lhs)?.assert_i32()?;
                let tb = self.parse_node(rhs)?.assert_i32()?;

                let t = self.assert_type(ta, tb)?;
                Ok(self.declare_type(*e, t))
            },
            Node::Let(e, vardecls) => {
                for vardecl in vardecls {
                    let e = vardecl.0;
                    let decl_type = self.get_type(e);

                    self.declare_var_type(e, decl_type.clone());

                    if let Some(ref var) = vardecl.1 {
                        let val_type = self.parse_node(&var)?;

                        self.assert_type(decl_type, val_type)?;
                    }
                }
                Ok(self.declare_type(*e, Type::Unit))
            },
            Node::If(e, cond, block, elseif, els) => {
                self.parse_node(cond)?.assert_bool()?;
                let ta = self.parse_node(block)?;
                let mut tb: Type;
                
                for ElseIf(cond, block) in elseif {
                    self.parse_node(cond)?.assert_bool()?;
                    tb = self.parse_node(block)?;

                    self.assert_type(ta.clone(), tb)?;
                }
                if let Some(Else(els)) = els {
                    tb = self.parse_node(els)?;

                    self.assert_type(ta.clone(), tb)?;
                }

                Ok(self.declare_type(*e, ta))
            },
            Node::Loop(e, block) => {
                let t = self.parse_node(block)?;
                Ok(self.declare_type(*e, t))
            },
            Node::Variable(entity) => {
                let t = self.get_var_type(*entity)?;
                Ok(self.declare_type(*entity, t))
            },
            Node::Print(e, val) => {
                self.parse_node(val)?;

                Ok(self.declare_type(*e, Type::Unit))
            },
            Node::Break(e, val) => {
                if let Some(val) = val {
                    self.parse_node(val)?;
                }
                Ok(self.declare_type(*e, Type::Unit))
            },
            Node::Literal(e) => {
                let literal = self.output.literals
                    .as_mut()
                    .expect("literals")
                    .get(&e)
                    .expect("literal");

                let t = match literal {
                    Literal::I32(_) => Type::I32,
                    Literal::Bool(_) => Type::Bool,
                    Literal::String(_) => Type::String
                };

                Ok(self.declare_type(*e, t))
            },
            Node::Unit(e) => {
                Ok(self.declare_type(*e, Type::Unit))
            }
        }
        //Ok(())
    }
}

impl <'a> PassTrait for Pass<'a> {
    type Output = self::Output<'a>;

    fn process(mut self) -> Result<Self::Output, Error> {
        let root = self.output.root.take().unwrap();
        self.parse_node(&root)?;
        self.output.root = Some(root);
        Ok(self.output)
    }
}
