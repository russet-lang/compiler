use failure::Fail;

#[derive(Debug, Fail)]
pub enum AstError {
    #[fail(display="Expected infix operator, found `{}`", found)]
    UnknownOperator {
        found: String
    }
}
