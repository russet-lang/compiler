use pest::iterators::Pair;
use crate::core::{
    WithConcreteAst
};

#[derive(Parser)]
#[grammar = "grammar.pest"]
pub struct Grammar;

pub struct Output<'a> (pub Pair<'a, Rule>);

impl<'a> WithConcreteAst<'a> for Output<'a> {
    fn get_concrete_ast(self) -> Pair<'a, Rule> {
        self.0
    }
}
