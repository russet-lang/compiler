use pest::Span;
use crate::core::{
    Component,
    WithTree,
    WithSpans,
    WithLiterals,
    WithVariables,
    WithTypes
};

use crate::types::*;
use crate::ast::output::{
    Node,
    Literal
};

#[derive(Default)]
pub struct Output<'a> {
    pub root: Option<Node>,
    pub spans: Option<Component<Span<'a>>>,
    pub literals: Option<Component<Literal<'a>>>,
    pub local_vars: Option<Component<Variable>>,
    pub types: Option<Component<Type>>,
}

impl<'a> WithTypes for Output<'a> {
    fn get_types(&mut self) -> Component<Type> {
        self.types.take().expect("get_types")
    }
}

impl<'a> WithTree for Output<'a> {
    fn get_tree(&mut self) -> Node {
        self.root.take().expect("get_tree")
    }
}

impl<'a> WithSpans<'a> for Output<'a> {
    fn get_spans(&mut self) -> Component<Span<'a>> {
        self.spans.take().expect("get_spans")
    }
}

impl<'a> WithLiterals<'a> for Output<'a> {
    fn get_literals(&mut self) -> Component<Literal<'a>> {
        self.literals.take().expect("get_literals")
    }
}

impl<'a> WithVariables for Output<'a> {
    fn get_variables(&mut self) -> Component<Variable> {
        self.local_vars.take().expect("local_vars")
    }
}

#[derive(Debug, Clone)]
pub struct Variable {
    pub pos: usize,
    pub size: usize
}
