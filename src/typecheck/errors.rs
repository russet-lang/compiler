use failure::Fail;

use crate::types::Type;

#[derive(Debug, Fail)]
pub enum TypeError {
    #[fail(display="Mismatched types `{}` != `{}`", left, right)]
    MismatchedTypes {
        left: Type,
        right: Type
    },

    #[fail(display="Expected type `{}` found `{}`", expected, found)]
    ExpectedType {
        expected: Type,
        found: Type
    }
}
