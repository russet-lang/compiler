use failure::Error;
use std::marker::PhantomData;
use std::iter::Peekable;
use std::collections::HashMap;
use pest::iterators::{Pair,Pairs};

use crate::core::{
    Pass as PassTrait,
    Entity,
    WithConcreteAst
};

//use crate::types::Type;
use crate::concrete::output::Rule;

pub mod output;
use self::output::*;

pub mod errors;
use self::errors::*;

#[allow(dead_code)]
#[derive(Clone, Copy, Debug)]
enum Assoc {
    Left,
    Right
}
struct Operator(Assoc, usize);

pub struct Pass<'a, P: 'a>
    where P: WithConcreteAst<'a>
{
    prev: Option<P>,
    phantom: PhantomData<&'a P>,
    prec: HashMap<Rule, Operator>,
    output: Output<'a>
}

impl<'a, P> Pass<'a, P>
    where P: WithConcreteAst<'a>
{
    pub fn new(prev: P) -> Self {
        let mut output = Output::default();
        output.root = None;
        output.spans = Some(HashMap::new());
        output.literals = Some(HashMap::new());
        output.types = Some(HashMap::new());
        output.entities = Some(0);

        Pass {
            prev: Some(prev),
            phantom: PhantomData,
            prec: hashmap!{
                Rule::assign => Operator(Assoc::Left, 1),
                Rule::lg => Operator(Assoc::Left, 10),
                Rule::add => Operator(Assoc::Left, 100),
                Rule::sub => Operator(Assoc::Left, 100),
                Rule::mul => Operator(Assoc::Left, 200),
                Rule::div => Operator(Assoc::Left, 200),
            },
            output
        }
    }

    fn create_entity(&mut self) -> Entity {
        let i = self.output.entities.unwrap();
        self.output.entities = Some(i + 1);
        i
    }

    fn add_span(&mut self, entity: Entity, pair: &Pair<'a, Rule>) {
        self.output.spans
            .as_mut()
            .expect("spans")
            .insert(entity, pair.as_span());
    }

    fn add_literal(&mut self, entity: Entity, literal: Literal<'a>) {
        self.output.literals.as_mut().unwrap().insert(entity, literal);
    }

    fn parse_infix_expr(&mut self, pair: Pair<'a, Rule>) -> Result<Node, Error> {
        let mut pairs = pair.into_inner().peekable();

        self.parse_infix_inner(&mut pairs, 1)
    }

    fn parse_infix_inner(&mut self, pairs: &mut Peekable<Pairs<'a, Rule>>, min_prec: usize) -> Result<Node, Error> {
        let first = pairs.next().expect("lhs");
        let mut lhs = self.parse_expr(first)?;

        while let Some(pair) = pairs.peek() {
            let rule = pair.as_rule();

            let op = self.prec.get(&rule);
            let Operator(assoc, prec) =
                op.ok_or(AstError::UnknownOperator {
                    found: pair.as_str().to_string()
                })?;
            if *prec < min_prec {
                break;
            }

            pairs.next();

            let next_min_prec = match assoc {
                Assoc::Left => *prec + 1,
                Assoc::Right => *prec
            };

            let rhs = self.parse_infix_inner(pairs, next_min_prec)?;

            let entity = self.create_entity();
            lhs = Node::Binary(entity, rule.into(), Box::new(lhs), Box::new(rhs));
        }

        Ok(lhs)
    }

    fn parse_unary_expr(&mut self, pair: Pair<'a, Rule>) -> Result<Node, Error> {
        let entity = self.create_entity();
        self.add_span(entity, &pair);

        let mut pairs = pair.into_inner();

        let op = pairs.next()
            .unwrap()
            .as_rule()
            .into();

        let expr = self.parse_expr(pairs.next().unwrap())?;
        Ok(Node::Unary(entity, op, Box::new(expr)))
    }

    fn parse_type(&mut self, entity: Entity, pair: Pair<'a, Rule>) {
        if let Rule::c_type = pair.as_rule() {
            let c_type = pair.into();

            self.output.types
                .as_mut()
                .expect("types")
                .insert(entity, c_type);
        }
        else {
            panic!("Expected type");
        }
    }

    fn parse_var_decl(&mut self, pair: Pair<'a, Rule>) -> Result<VarDecl, Error> {
        let entity = self.create_entity();

        let mut pairs = pair.into_inner();
        let pair = pairs.next().unwrap();
        if let Rule::variable = pair.as_rule() {
            self.add_span(entity, &pair);

            let type_pair = pairs.next().unwrap();

            self.parse_type(entity, type_pair);

            let value = pairs
                .next()
                .map(|pair| self.parse_expr(pair))
                .map_or(Ok(None), |v| v.map(Box::new).map(Some))?;

            Ok(VarDecl(entity, value))
        }
        else {
            panic!("Expected variable declaration, found: {}", pair)
        }
    }

    fn parse_if(&mut self, pair: Pair<'a, Rule>) -> Result<Node, Error> {
        let entity = self.create_entity();
        self.add_span(entity, &pair);

        let mut pairs = pair.into_inner();
        let cond = self.parse_expr(pairs.next().unwrap())?;
        let block = self.parse_expr(pairs.next().unwrap())?;

        let mut elsif = vec![];
        let mut els = None;

        for pair in pairs {
            match pair.as_rule() {
                Rule::c_elseif => {
                    let mut pairs = pair.into_inner();
                    let cond = self.parse_expr(pairs.next().unwrap())?;
                    let block = self.parse_expr(pairs.next().unwrap())?;
                    elsif.push(ElseIf(Box::new(cond), Box::new(block)));
                },
                Rule::c_else => {
                    let mut pairs = pair.into_inner();
                    let block = self.parse_expr(pairs.next().unwrap())?;
                    els = Some(Else(Box::new(block)));
                },
                r => panic!("Expected else or elseif, found: {:?}", r)
            }
        }

        Ok(Node::If(entity, Box::new(cond), Box::new(block), elsif, els))
    }

    fn parse_letvar(&mut self, pair: Pair<'a, Rule>) -> Result<Node, Error> {
        let entity = self.create_entity();
        self.add_span(entity, &pair);

        let pairs = pair.into_inner();
        let (vardecls, errs): (Vec<_>, Vec<_>) = pairs
            .map(|pair| self.parse_var_decl(pair))
            .partition(Result::is_ok);

        for err in errs {
            err?;
        }

        let vardecls = vardecls.into_iter().map(Result::unwrap).collect();

        Ok(Node::Let(entity, vardecls))
    }

    fn parse_expr(&mut self, pair: Pair<'a, Rule>) -> Result<Node, Error> {
        match pair.as_rule() {
            Rule::number => {
                let entity = self.create_entity();
                self.add_span(entity, &pair);

                let i = pair.as_str().parse().expect("i32 parse");

                self.add_literal(entity, Literal::I32(i));

                Ok(Node::Literal(entity))
            },
            Rule::boolean => {
                let entity = self.create_entity();
                let b = pair.as_str() == "true";
                self.add_span(entity, &pair);
                self.add_literal(entity, Literal::Bool(b));
                Ok(Node::Literal(entity))
            },
            Rule::string => {
                let entity = self.create_entity();
                self.add_span(entity, &pair);
                let inner = pair.into_inner().next().expect("inner string");
                self.add_literal(entity, Literal::String(inner.as_str()));
                Ok(Node::Literal(entity))
            },
            Rule::expr_infix => self.parse_infix_expr(pair),
            Rule::expr_unary => self.parse_unary_expr(pair),
            Rule::block => self.parse_block(pair),
            Rule::c_if => self.parse_if(pair),
            Rule::c_loop => self.parse_loop(pair),
            Rule::c_break => {
                let entity = self.create_entity();
                self.add_span(entity, &pair);
                let expr = pair.into_inner()
                    .next()
                    .map(|n| self.parse_expr(n))
                    .map_or(Ok(None), |v| v.map(Box::new).map(Some))?;

                Ok(Node::Break(entity, expr))
            }
            Rule::letvar => self.parse_letvar(pair),
            Rule::print => {
                let entity = self.create_entity();
                self.add_span(entity, &pair);
                let node = pair.into_inner().next().unwrap();
                let node = self.parse_expr(node)?;
                Ok(Node::Print(entity, Box::new(node)))
            },
            Rule::unit => {
                let entity = self.create_entity();
                self.add_span(entity, &pair);
                Ok(Node::Unit(entity))
            },
            Rule::variable => {
                let entity = self.create_entity();
                self.add_span(entity, &pair);
                Ok(Node::Variable(entity))
            },
            Rule::tuple => self.parse_tuple(pair),
            r => unimplemented!("{:?}", r)
        }
    }

    fn parse_tuple(&mut self, pair: Pair<'a, Rule>) -> Result<Node, Error> {
        let entity = self.create_entity();
        self.add_span(entity, &pair);
        let (nodes, errs) : (Vec<_>, Vec<_>) = pair.into_inner()
            .map(|p| self.parse_expr(p))
            .partition(Result::is_ok);

        let nodes = nodes.into_iter().map(Result::unwrap).collect();
        for err in errs {
            err?;
        }

        Ok(Node::Tuple(entity, nodes))
    }

    fn parse_exprs(&mut self, pair: Pair<'a, Rule>) -> Result<Node, Error> {
        if let Rule::exprs = pair.as_rule() {
            let entity = self.create_entity();
            self.add_span(entity, &pair);
            let (nodes, errs) : (Vec<_>, Vec<_>) = pair.into_inner()
                .map(|p| self.parse_expr(p))
                .partition(Result::is_ok);

            let nodes = nodes.into_iter().map(Result::unwrap).collect();
            for err in errs {
                err?;
            }

            Ok(Node::Block(entity, nodes))
        }
        else {
            panic!("Expected block found {:?}", pair.as_rule());
        }
    }

    fn parse_block(&mut self, pair: Pair<'a, Rule>) -> Result<Node, Error> {
        let entity = self.create_entity();
        self.add_span(entity, &pair);

        let mut pairs = pair.into_inner();
        match pairs.next() {
            Some(pair) => self.parse_exprs(pair),
            None => {
                Ok(Node::Block(entity, vec![]))
            }
        }
    }

    fn parse_loop(&mut self, pair: Pair<'a, Rule>) -> Result<Node, Error> {
        let entity = self.create_entity();
        self.add_span(entity, &pair);

        let expr = self.parse_expr(pair.into_inner().next().unwrap())?;

        Ok(Node::Loop(entity, Box::new(expr)))
    }

}

impl <'a, P> PassTrait for Pass<'a, P>
where P: WithConcreteAst<'a> {
    type Output = self::Output<'a>;

    fn process(mut self) -> Result<Self::Output, Error> {
        let ast = self.prev
            .take()
            .unwrap()
            .get_concrete_ast();

        let root = self.parse_exprs(ast)?;

        self.output.root = Some(root);
        Ok(self.output)
    }
}
