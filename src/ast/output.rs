use pest::Span;
use crate::core::{
    Entity,
    Component,
    WithTree,
    WithSpans,
    WithLiterals,
    WithTypes,
    WithEntities
};
use crate::types::Type;
use crate::concrete::output::Rule;


#[derive(Default)]
pub struct Output<'a> {
    pub root: Option<Node>,
    pub spans: Option<Component<Span<'a>>>,
    pub literals: Option<Component<Literal<'a>>>,
    pub types: Option<Component<Type>>,
    pub entities: Option<usize>
}

impl<'a> WithEntities for Output<'a> {
    fn get_entities(&mut self) -> usize {
        self.entities.take().expect("get_entities")
    }
}

impl<'a> WithTree for Output<'a> {
    fn get_tree(&mut self) -> Node {
        self.root.take().expect("get_tree")
    }
}

impl<'a> WithSpans<'a> for Output<'a> {
    fn get_spans(&mut self) -> Component<Span<'a>> {
        self.spans.take().expect("get_spans")
    }
}

impl<'a> WithLiterals<'a> for Output<'a> {
    fn get_literals(&mut self) -> Component<Literal<'a>> {
        self.literals.take().expect("get_literals")
    }
}

impl<'a> WithTypes for Output<'a> {
    fn get_types(&mut self) -> Component<Type> {
        self.types.take().expect("get_types")
    }
}

#[derive(Debug)]
pub enum Node {
    Literal(Entity),
    Unit(Entity),
    Tuple(Entity, Vec<Node>),
    Unary(Entity, UnaryOp, Box<Node>),
    Binary(Entity, BinaryOp, Box<Node>, Box<Node>),

    Block(Entity, Vec<Node>),
    Let(Entity, Vec<VarDecl>),
    If(Entity, Box<Node>, Box<Node>, Vec<ElseIf>, Option<Else>), // cond, expr
    Loop(Entity, Box<Node>),
    Break(Entity, Option<Box<Node>>),
    Print(Entity, Box<Node>),
    Variable(Entity)
}

#[derive(Debug)]
pub struct ElseIf(pub Box<Node>, pub Box<Node>); // cond, expr

#[derive(Debug)]
pub struct Else(pub Box<Node>);

#[derive(Debug)]
pub struct VarDecl(pub Entity, pub Option<Box<Node>>);


#[derive(Debug)]
pub enum Literal<'a> {
    I32(i32),
    Bool(bool),
    String(&'a str)
}

#[derive(Debug)]
pub enum BinaryOp {
    Add,
    Sub,
    Mul,
    Div,
    Assign,
    Larger
}

impl From<Rule> for BinaryOp {
    fn from(rule: Rule) -> Self {
        match rule {
            Rule::add => BinaryOp::Add,
            Rule::sub => BinaryOp::Sub,
            Rule::mul => BinaryOp::Mul,
            Rule::div => BinaryOp::Div,
            Rule::assign => BinaryOp::Assign,
            Rule::lg => BinaryOp::Larger,
            r => panic!("Rule {:?} is not binary operator", r)
        }
    }
}

#[derive(Debug)]
pub enum UnaryOp {
    Not,
    Minus
}

impl From<Rule> for UnaryOp {
    fn from(rule: Rule) -> Self {
        match rule {
            Rule::not => UnaryOp::Not,
            Rule::sub => UnaryOp::Minus,
            r => panic!("Rule {:?} is not unary operator", r)
        }
    }
}
