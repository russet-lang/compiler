use failure::Error;
use std::marker::PhantomData;
use std::collections::HashMap;
use ::core::bytecode::Bytecode;
use crate::core::{
    Pass as PassTrait,
    Entity,
    Component,
    WithTree,
    WithTypes,
    WithLiterals,
    WithVariables
};
use crate::ast::output::*;

use crate::local_vars::output::Variable;
use crate::types::*;

enum EffectType {
    Print, //0
    Not, //1
    Add, //2
    Sub, //3
    Minus, //4
    Mul, //5
    Div,  //6
    Larger,  //7
}

type Label = usize;

pub struct Pass<'a>
{
    root: Option<Node>,
    code: Vec<Bytecode>,
    literals: Option<Component<Literal<'a>>>,
    variables: Option<Component<Variable>>,
    types: Option<Component<Type>>,
    phantom: PhantomData<&'a Literal<'a>>,
    labels: HashMap<Label, (Vec<usize>, Option<usize>)>,
    label_count: usize,
    break_stack: Vec<usize>
}

impl <'a> Pass<'a> {
    pub fn new<P>(mut prev: P) -> Self
    where P: WithTree
        + WithTypes
        + WithLiterals<'a>
        + WithVariables {
        let root = prev.get_tree();
        let literals = prev.get_literals();
        let variables = prev.get_variables();
        let types = prev.get_types();
        Pass {
            root: Some(root),
            literals: Some(literals),
            variables: Some(variables),
            types: Some(types),
            phantom: PhantomData,
            code: vec![],
            labels: HashMap::new(),
            label_count: 0,
            break_stack: vec![]
        }
    }

    fn get_type(&self, node: &Node) -> Type {
        let e = self.get_entity(node);

        println!("Node: {:?}", node);
        println!("Entity: {:?}", e);
        println!("TYPES: {:?}", self.types);
        
        let t = self.types
            .as_ref()
            .unwrap()
            .get(&e)
            .unwrap();
        
        t.clone()
    }

    fn get_entity(&self, node: &Node) -> Entity {
        *match node {
            Node::Block(e, _) => e,
            Node::Literal(e) => e,
            Node::Unary(e, _, _) => e,
            Node::Binary(e, _, _, _) => e,
            Node::Variable(e) => e,
            Node::Let(e, _) => e,
            Node::If(e, _, _, _, _) => e,
            Node::Loop(e, _) => e,
            Node::Break(e, _) => e,
            Node::Print(e, _) => e,
            Node::Unit(e) => e,
            Node::Tuple(e, _) => e,
        }
    }

    fn add_byte(&mut self, byte: Bytecode) {
        self.code.push(byte);
    }

    fn add_effect(&mut self, effect: EffectType, argc: usize) {
        self.add_byte(Bytecode::PushEffect(effect as usize, argc));
        self.add_byte(Bytecode::Yield);
    }

    fn new_label(&mut self) -> Label {
        let lc = self.label_count;
        self.label_count += 1;

        self.labels.insert(lc, (vec![], None));

        lc
    }

    fn add_label(&mut self, label: Label) {
        let label_loc = self.code.len();
        let locs = self.labels.get_mut(&label).expect("label metadata");
        locs.1 = Some(label_loc);
    }

    fn patch_labels(&mut self) {
        for (_, (locs, label_loc)) in self.labels.iter() {
            let label_loc = label_loc.expect("Label location");
            for loc in locs.iter() {
                let byte = match &self.code[*loc] {
                    Bytecode::Jump(_) => Bytecode::Jump(label_loc),
                    b => unimplemented!("{:?} for label patching", b)
                };
                self.code[*loc] = byte;
            }
        }
    }

    fn add_labeled(&mut self, byte: Bytecode, label: Label) {
        let locs = self.labels.get_mut(&label).expect("label metadata");
        let loc = self.code.len();

        locs.0.push(loc);
        self.add_byte(byte);
    }

    fn parse_loop(&mut self, block: &Node) {
        let begin = self.new_label();
        let end = self.new_label();
        self.break_stack.push(end);
        
        self.add_label(begin);
        self.parse_node(block);

        self.add_labeled(Bytecode::Jump(0), begin);
        self.add_label(end);
    }

    fn parse_if(&mut self, cond: &Node, block: &Node, elseif: &[ElseIf], els: &Option<Else>) {
        self.parse_node(cond);
        self.add_byte(Bytecode::SkipTrue);

        let mut false_label = self.new_label();
        self.add_labeled(Bytecode::Jump(0), false_label);

        self.parse_node(block);

        let end_if = self.new_label();
        self.add_labeled(Bytecode::Jump(0), end_if);

        for el in elseif {
            self.add_label(false_label);
            self.parse_node(&el.0); //cond
            self.add_byte(Bytecode::SkipTrue);

            false_label = self.new_label();
            self.add_labeled(Bytecode::Jump(0), false_label);

            self.parse_node(&el.1); //block

            self.add_labeled(Bytecode::Jump(0), end_if);
        }

        self.add_label(false_label);

        if let Some(els) = els {
            self.parse_node(&els.0);
        }

        self.add_label(end_if);
    }

    fn parse_node(&mut self, node: &Node) {
        match node {
            Node::Block(_e, inner) => {
                for node in inner {
                    self.parse_node(node);
                }
            },
            Node::Literal(e) => {
                let literal: Bytecode = self.literals
                    .as_mut()
                    .expect("literals")
                    .get(&e)
                    .expect("literal").into();
                self.add_byte(literal);
            },
            Node::Unary(_e, UnaryOp::Not, val) => {
                self.parse_node(val);
                self.add_effect(EffectType::Not, 1);
            },
            Node::Unary(_e, UnaryOp::Minus, val) => {
                self.parse_node(val);
                self.add_effect(EffectType::Minus, 1);
            },
            Node::Binary(_e, BinaryOp::Add, a, b) => {
                self.parse_node(a);
                self.parse_node(b);
                self.add_effect(EffectType::Add, 2);
            },
            Node::Binary(_e, BinaryOp::Sub, a, b) => {
                self.parse_node(a);
                self.parse_node(b);
                self.add_effect(EffectType::Sub, 2);
            },
            Node::Binary(_e, BinaryOp::Mul, a, b) => {
                self.parse_node(a);
                self.parse_node(b);
                self.add_effect(EffectType::Mul, 2);
            },
            Node::Binary(_e, BinaryOp::Div, a, b) => {
                self.parse_node(a);
                self.parse_node(b);
                self.add_effect(EffectType::Div, 2);
            },
            Node::Binary(_e, BinaryOp::Larger, a, b) => {
                self.parse_node(a);
                self.parse_node(b);
                self.add_effect(EffectType::Larger, 2);
            },
            Node::Binary(_e, BinaryOp::Assign, box Node::Variable(e), value) => {
                self.parse_node(value);
                let pos = self.variables
                    .as_mut()
                    .expect("variables")
                    .get(&e)
                    .expect("assign variable").pos;

                self.add_byte(Bytecode::Save(pos));
            },
            Node::Binary(_e, _, _, _) => {
                unreachable!();
            },
            Node::Variable(e) => {
                let pos = self.variables
                    .as_mut()
                    .expect("variables")
                    .get(&e)
                    .expect("variable").pos;

                let c_type = self.get_type(node);
                if let Type::Tuple(inner) = c_type {
                    println!("INNER: {:?}", inner);
                    //self.add_effect(EffectType::Print, inner.len());
                    let mut pos = pos;
                    for i in inner {
                        self.add_byte(Bytecode::Load(pos));
                        pos += 1;
                    }
                }
                else {
                    self.add_byte(Bytecode::Load(pos));
                }
            }
            Node::Let(_e, vardecls) => {
                for vardecl in vardecls {
                    match vardecl.1 {
                        None => self.add_byte(Bytecode::PushI32(0)),
                        Some(ref var) => self.parse_node(&var)
                    }
                }
            },
            Node::If(_e, cond, block, elseif, els) => {
                self.parse_if(cond, block, elseif, els);
            },
            Node::Loop(_e, block) => {
                self.parse_loop(block);
            },
            Node::Break(_e, value) => {
                let label = self.break_stack.pop().expect("Break must be inside loop");

                if let Some(value) = value {
                    self.parse_node(value);
                }

                self.add_labeled(Bytecode::Jump(0), label);
            },
            Node::Print(_e, node) => {
                self.parse_node(node);
                let c_type = self.get_type(node);
                match c_type {
                    Type::Unit => (),
                    Type::Tuple(inner) => {
                        self.add_effect(EffectType::Print, inner.len());
                        self.add_byte(Bytecode::Pop);
                    },
                    _ => {
                        self.add_byte(Bytecode::Print);
                        self.add_byte(Bytecode::Pop);
                    }
                }
            },
            Node::Unit(_) => {
            },
            Node::Tuple(_, inner) => {
                for node in inner {
                    self.parse_node(node);
                }
            }
        }
    }

    fn parse_root(&mut self) {
        let root = self.root.take().unwrap();
        self.parse_node(&root);
        self.root = Some(root);
    }
}

impl <'a> PassTrait for Pass<'a> {
    type Output = Vec<Bytecode>;

    fn process(mut self) -> Result<Self::Output, Error> {
        self.parse_root();

        self.patch_labels();

        Ok(self.code)
    }
}

impl<'a> From<&'a Literal<'a>> for Bytecode {
    fn from(literal: &'a Literal<'a>) -> Self {
        match literal {
            Literal::I32(i) => {
                Bytecode::PushI32(*i)
            },
            Literal::Bool(b) => {
                Bytecode::PushBool(*b)
            },
            Literal::String(s) => {
                Bytecode::PushString(s.to_string())
            }
        }
    }
}
