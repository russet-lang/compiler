#![feature(bind_by_move_pattern_guards, box_patterns)]

#[macro_use] extern crate pest_derive;
#[macro_use] extern crate maplit;

mod core;
mod types;

mod concrete;
mod ast;
mod var_bindings;
mod typecheck;
mod local_vars;
mod codegen;

use failure::Error;
use crate::core::Pass;
use self::concrete::Pass as Concrete;
use self::ast::Pass as Ast;
use self::var_bindings::Pass as VarBindings;
use self::typecheck::Pass as TypeCheck;
use self::local_vars::Pass as LocalVars;
use self::codegen::Pass as Codegen;

use ::core::bytecode::Bytecode;
pub fn compile(input: &str) -> Result<Vec<Bytecode>, Error> {
    let concrete = Concrete::new(input).process()?;
    let ast = Ast::new(concrete).process()?;
    let var_bindings = VarBindings::new(ast).process()?;
    let typecheck = TypeCheck::new(var_bindings).process()?;
    let local_vars = LocalVars::new(typecheck).process()?;
    Codegen::new(local_vars).process()
}









#[allow(dead_code)]
fn test_data() -> Vec<Bytecode> {
    vec![
        // /* 00 */ Bytecode::Comment("BEGIN".into()),
        /* 00 */
        Bytecode::PushString("Before handler (compiler!)".into()),
        /* 01 */ Bytecode::Print,
        /* 02 */ Bytecode::Pop,
        // /* 04 */ Bytecode::Comment("TRY".into()),
        /* 03 */
        Bytecode::SpawnAwait(17),
        /* 04 */ Bytecode::IsAlive,
        /* 05 */ Bytecode::SkipTrue,
        /* 06 */ Bytecode::Jump(23),
        // /* 09 */ Bytecode::Comment("HANDLE".into()),
        /* 07 */
        Bytecode::Handle(2, 11),
        /* 08 */ Bytecode::Yield,
        /* 09 */ Bytecode::Continue,
        /* 10 */ Bytecode::Jump(4),
        // /* 14 */ Bytecode::Comment("EFFECT 2".into()),
        /* 11 */
        Bytecode::Print,
        /* 12 */ Bytecode::Pop,
        /* 13 */ Bytecode::PushString("Value from effect handler".into()),
        /* 14 */ Bytecode::PushArgc(1),
        /* 15 */ Bytecode::Continue,
        /* 16 */ Bytecode::Jump(4),
        // /* 21 */ Bytecode::Comment("FIBER 2".into()),
        /* 17 */
        Bytecode::PushString("Value from try".into()),
        /* 18 */ Bytecode::PushEffect(2, 1),
        /* 19 */ Bytecode::Yield,
        /* 20 */ Bytecode::Print,
        // /* 21 */ Bytecode::Read,
        // /* 22 */ Bytecode::Print,
        /* 23 */ Bytecode::Halt,
    ]
}
