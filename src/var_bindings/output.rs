use pest::Span;
use crate::core::{
    Component,
    Entity,
    WithTree,
    WithSpans,
    WithLiterals,
    WithEntities,
    WithTypes,
    WithVarBindings
};

use crate::ast::output::{
    Node,
    Literal
};
use crate::types::Type;

#[derive(Default)]
pub struct Output<'a> {
    pub root: Option<Node>,
    pub spans: Option<Component<Span<'a>>>,
    pub literals: Option<Component<Literal<'a>>>,
    pub types: Option<Component<Type>>,
    pub entities: Option<usize>,
    pub var_bindings: Option<Component<VarBinding>>
    //pub local_vars: Option<Component<Variable>>
}

impl<'a> WithTypes for Output<'a> {
    fn get_types(&mut self) -> Component<Type> {
        self.types.take().expect("get_types")
    }
}

impl<'a> WithEntities for Output<'a> {
    fn get_entities(&mut self) -> usize {
        self.entities.take().expect("get_entities")
    }
}

impl<'a> WithTree for Output<'a> {
    fn get_tree(&mut self) -> Node {
        self.root.take().expect("get_tree")
    }
}

impl<'a> WithSpans<'a> for Output<'a> {
    fn get_spans(&mut self) -> Component<Span<'a>> {
        self.spans.take().expect("get_spans")
    }
}

impl<'a> WithLiterals<'a> for Output<'a> {
    fn get_literals(&mut self) -> Component<Literal<'a>> {
        self.literals.take().expect("get_literals")
    }
}

impl<'a> WithVarBindings for Output<'a> {
    fn get_var_bindings(&mut self) -> Component<VarBinding> {
        self.var_bindings.take().expect("var_bindings")
    }
}

#[derive(Debug, Copy, Clone)]
pub struct VarBinding(pub Entity);
    //pub pos: usize
//}
