use pest::iterators::Pair;
use crate::concrete::output::Rule;
use failure::Error;

pub type Entity = usize;

pub trait Pass {
    type Output;
    fn process(self) -> Result<Self::Output, Error>;
}

use std::collections::HashMap;
pub type Component<T> = HashMap<Entity, T>;

pub trait WithConcreteAst<'a> {
    fn get_concrete_ast(self) -> Pair<'a, Rule>;
}

use crate::ast::output::{Node, Literal};

pub trait WithTree {
    fn get_tree(&mut self) -> Node;
}

pub trait WithEntities {
    fn get_entities(&mut self) -> usize;
}

use pest::Span;

pub trait WithSpans<'a> {
    fn get_spans(&mut self) -> Component<Span<'a>>;
}

pub trait WithLiterals<'a> {
    fn get_literals(&mut self) -> Component<Literal<'a>>;
}

use crate::types::Type;
pub trait WithTypes {
    fn get_types(&mut self) -> Component<Type>;
}

use crate::var_bindings::output::{VarBinding};
pub trait WithVarBindings {
    fn get_var_bindings(&mut self) -> Component<VarBinding>;
}

use crate::local_vars::output::{Variable};
pub trait WithVariables {
    fn get_variables(&mut self) -> Component<Variable>;
}

