#[derive(Debug, PartialEq, Clone)]
pub enum Type {
    I32,
    Bool,
    String,
    Unit,
    Tuple(Vec<Type>)
}

use std::fmt;
use pest::iterators::Pair;

use crate::typecheck::errors::*;
use crate::concrete::output::Rule;

impl<'a> From<Pair<'a, Rule>> for Type {
    fn from(pair: Pair<'a, Rule>) -> Self {
        let rule = pair.as_rule();
        match rule {
            Rule::c_type => {
                let inner = pair.into_inner().next().unwrap();
                inner.into()
            },
            Rule::i32_type => Type::I32,
            Rule::bool_type => Type::Bool,
            Rule::string_type => Type::String,
            Rule::unit_type => Type::Unit,
            Rule::tuple_type => {
                let ts : Vec<Type> = pair
                    .into_inner()
                    .map(|p| p.into())
                    .collect();

                if ts.len() == 0 {
                    Type::Unit
                }
                else if ts.len() == 1 {
                    ts[0].clone()
                }
                else {
                    Type::Tuple(ts)
                }
            }
            r => unimplemented!("{:?}", r)
        }
    }
}

impl Type {
    pub fn assert(self, expected: Type) -> Result<Self, TypeError> {
        if self == expected { Ok(self) }
        else {
            Err(TypeError::ExpectedType {
                expected,
                found: self
            })
        }
    }

    pub fn assert_i32(self) -> Result<Self, TypeError> {
        self.assert(Type::I32)
    }

    pub fn assert_bool(self) -> Result<Self, TypeError> {
        self.assert(Type::Bool)
    }
}

impl fmt::Display for Type {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
         match self {
            Type::I32 => write!(f, "i32"),
            Type::Bool => write!(f, "bool"),
            Type::String => write!(f, "string"),
            Type::Unit => write!(f, "()"),
            Type::Tuple(ts) => {
                let mut s = "(".to_string();
                for t in &ts[0 .. ts.len() - 1] {
                    s += &format!("{}, ", t);
                }
                s += &format!("{})", &ts[ts.len() - 1]);
                write!(f, "{}", s)
            }
        }
    }
}
