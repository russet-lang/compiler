use failure::Error;
use std::collections::HashMap;
use crate::core::{
    Pass as PassTrait,
    Component,
    Entity,
    WithTree,
    WithSpans,
    WithLiterals,
    WithTypes,
    WithVarBindings
};

use crate::ast::output::{Node, Else, ElseIf};
use crate::var_bindings::output::{VarBinding};
use crate::types::Type;

pub mod output;
use self::output::*;

pub struct Pass<'a>
{
    output: Output<'a>,
    stack: usize,
    var_bindings: Component<VarBinding>
}

impl <'a> Pass<'a> {
    pub fn new<P>(mut prev: P) -> Self
    where P: WithTree
        + WithVarBindings
        + WithTypes
        + WithSpans<'a>
        + WithLiterals<'a> {
        let mut output = Output::default();
        output.root = Some(prev.get_tree());
        output.spans = Some(prev.get_spans());
        output.literals = Some(prev.get_literals());
        output.local_vars = Some(HashMap::new());
        output.types = Some(prev.get_types());
        Pass {
            output,
            var_bindings: prev.get_var_bindings(),
            stack: 0
        }
    }

    fn add_var(&mut self, entity: Entity, var: Variable) {
        self.output
            .local_vars
            .as_mut()
            .unwrap()
            .insert(entity, var);
    }

    fn get_binding(&self, entity: Entity) -> &VarBinding {
        self.var_bindings
            .get(&entity)
            .expect("get_binding")
    }

    fn add_to_stack(&mut self, entity: Entity, size: usize) {
        let pos = self.stack;

        let VarBinding(binding) = self.get_binding(entity);

        self.add_var(*binding, Variable { pos, size });

        self.stack += size;
    }

    fn get_var(&mut self, entity: Entity) -> Result<Variable, Error> {
        let binding = {self.get_binding(entity).0};

        let var = self.output
            .local_vars
            .as_mut()
            .unwrap()
            .get(&binding)
            .unwrap()
            .clone();

        Ok(var)
    }

    fn get_type(&self, e: Entity) -> Type {
        self.output.types
            .as_ref()
            .expect("types")
            .get(&e)
            .expect("type")
            .clone()
    }

    fn get_var_size(&self, t: Type) -> usize {
        match t {
            Type::Tuple(inner) => inner.len(),
            _ => 1
        }
    }

    fn parse_node(&mut self, node: &Node) -> Result<(), Error> {
        match node {
            Node::Block(_, inner) => {
                for node in inner {
                    self.parse_node(node)?;
                }
            },
            Node::Tuple(_, inner) => {
                for node in inner {
                    self.parse_node(node)?;
                }
            },
            Node::Unary(_, _, inner) => {
                self.parse_node(inner)?;
            },
            Node::Binary(_, _, lhs, rhs) => {
                self.parse_node(lhs)?;
                self.parse_node(rhs)?;
            },
            Node::Let(_, vardecls) => {
                for vardecl in vardecls {
                    let e = vardecl.0;

                    let decl_type = self.get_type(e);
                    let size = self.get_var_size(decl_type);
                    self.add_to_stack(e, size);

                    if let Some(ref var) = vardecl.1 {
                        self.parse_node(&var)?;
                    }
                }
            },
            Node::If(_, cond, block, elseif, els) => {
                self.parse_node(cond)?;
                self.parse_node(block)?;
                for ElseIf(cond, block) in elseif {
                    self.parse_node(cond)?;
                    self.parse_node(block)?;
                }
                if let Some(Else(els)) = els {
                    self.parse_node(els)?;
                }
            },
            Node::Loop(_, block) => {
                self.parse_node(block)?;
            },
            Node::Variable(entity) => {
                let var = self.get_var(*entity)?;
                self.add_var(*entity, var);
            },
            Node::Print(_, e) => {
                self.parse_node(e)?;
            },
            Node::Break(_, e) => {
                if let Some(e) = e {
                    self.parse_node(e)?;
                }
            },
            Node::Literal(_) => (),
            Node::Unit(_) => (),
        }
        Ok(())
    }
}

impl <'a> PassTrait for Pass<'a> {
    type Output = self::Output<'a>;

    fn process(mut self) -> Result<Self::Output, Error> {
        let root = self.output.root.take().unwrap();
        self.parse_node(&root)?;
        self.output.root = Some(root);
        Ok(self.output)
    }
}
