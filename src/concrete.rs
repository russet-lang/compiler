use pest::Parser;
use failure::Error;

use crate::core::Pass as PassTrait;

pub mod output;
use self::output::*;

pub struct Pass<'a> {
    input: &'a str
}

impl<'a> Pass<'a> {
    pub fn new(input: &'a str) -> Self {
        Pass {
            input
        }
    }
}

impl <'a> PassTrait for Pass<'a> {
    type Output = self::Output<'a>;

    fn process(self) -> Result<Self::Output, Error> {
        let ast = Grammar::parse(Rule::program, self.input)?
            .next().expect("AST root");

        Ok(Output(ast))
    }
}
